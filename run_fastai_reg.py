import config
from data_processing import databunch as db
from models import fastai
from pipeline import single_model_pipeline

databunch = db.get_fastai_databunch(seed=config.SEED, mode='reg', lm_vocab_path=config.lm_vocab_path)
model = fastai.get_fastai_regressor(databunch=databunch, encoder_path=config.encoder_path)
model = single_model_pipeline.train_fastai(learn=model)
model.model_dir = config.model_weights_save_path
model.save("fastai_reg")

single_model_pipeline.predict_all_reg_fastai(model)

# print(model.path)
# print(model.model_dir)
#
# model.path = Path("./data/fastai_cls")
# model = model.load("fastai_clf")
# model.model_dir = config.model_weights_save_path
# single_model_pipeline.predict_all_fastai(model)
