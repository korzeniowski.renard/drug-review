import config
from data_processing import databunch as db
from models import fastai
from pipeline import single_model_pipeline

databunch = db.get_fastai_lm_databunch(seed=config.SEED, mode='cls')
model = fastai.get_fastai_lm(databunch=databunch)
single_model_pipeline.train_fastai_lm(learn=model)
