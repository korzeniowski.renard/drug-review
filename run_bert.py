from pipeline import single_model_pipeline

exp = single_model_pipeline.Experiment.from_bert_cls()
exp.train_model()

# checkpoint_model_path = "./data/bert_featdrop0d2_minfreq5/model_weights.pytorch"
# exp = single_model_pipeline.Experiment.from_bert_checkpoint(checkpoint_model_path=checkpoint_model_path)
exp.predict_all()
