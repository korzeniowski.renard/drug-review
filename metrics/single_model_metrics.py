import torch

# TODO
# - add roc curve
# - prec
# - recall


class BinaryAccuracy:
    name = 'accuracy'

    def get_metric(self, preds, y):
        preds = torch.argmax(preds, dim=1)
        correct = (preds == y).float()
        acc = correct.sum() / len(correct)
        return acc
