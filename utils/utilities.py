import numpy as np
import torch
import random
import config


def get_device():
    return torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def set_random_seed(seed_value=config.SEED):
    np.random.seed(seed_value) # cpu vars
    torch.manual_seed(seed_value) # cpu  vars
    random.seed(seed_value) # Python
    if get_device() == "cuda:0":
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value) # gpu vars
        torch.backends.cudnn.deterministic = True  #needed
        torch.backends.cudnn.benchmark = False