import pandas as pd
from collections import defaultdict

class ReplaceKnownRatings:
    def __init__(self, predictions_df, train_df):
        self.original_predictions_df = predictions_df.copy()
        self.predictions_df = self.preprocess_strings(predictions_df)
        self.train_df = self.preprocess_strings(train_df)
        self.train_opinions = self.group_opinions(self.train_df)
        self.corrected_predictions_df = predictions_df.copy()
    
    def get_corrected_predictions_for_column(self, min_opinion_length=1, max_nr_of_different_ratings=2, rating_column='rate', replace_with='mean'):
        """ `replace_with` options:
                'mean' and 'median' for rating_column=='rate'
                ignored for rating_column == 'rate1'. 'mode' (most common value) is used
        """
        if rating_column == 'rate1':
            replace_with = 'mode'
            
        opinions_to_replace = self.train_opinions[self.train_opinions.index.str.len()>=min_opinion_length]
        opinions_to_replace = opinions_to_replace[opinions_to_replace[rating_column+'_nunique']<=max_nr_of_different_ratings]
        df = opinions_to_replace[rating_column+'_'+replace_with]
        df.columns = [rating_column]
        map_opinion_rate = defaultdict(lambda: None, df.to_dict())
        
        should_replace = self.predictions_df.opinion.isin(df.index)
        preds_to_replace = self.predictions_df.opinion.apply(lambda x: map_opinion_rate[x])
        new_predictions = self.corrected_predictions_df.copy()
        new_predictions.loc[should_replace, rating_column] = preds_to_replace[should_replace]
        self.corrected_predictions_df = new_predictions.copy()
        return new_predictions

    def get_corrected_predictions(self, min_opinion_length=1, max_nr_of_different_ratings=2, replace_with='mean'):
        self.get_corrected_predictions_for_column(min_opinion_length=min_opinion_length, max_nr_of_different_ratings=max_nr_of_different_ratings, replace_with=replace_with, rating_column='rate')
        self.get_corrected_predictions_for_column(min_opinion_length=min_opinion_length, max_nr_of_different_ratings=max_nr_of_different_ratings, rating_column='rate1')
        return self.corrected_predictions_df

    def preprocess_strings(self, df):
        to_replace = '[^0-9a-zA-Z]'
        def preprocess_column(series):
            return series.str.replace('&#039;', '').str.replace(to_replace, '').str.lower().str.strip()
        
        new_df = df.copy()       
        new_df.name = preprocess_column(new_df.name)
        new_df.condition = preprocess_column(new_df.condition)
        new_df.opinion = preprocess_column(new_df.opinion)
        return new_df
    
    def group_opinions(self, df):
        def mode(x):
            return pd.Series.mode(x)[0]
        per_opinion = df.groupby('opinion').agg({'rate': ['count','mean', 'nunique', 'median'],
                                                 'rate1': ['count', 'nunique', mode]})
        per_opinion.columns = ['_'.join(col).strip() for col in per_opinion.columns.values]
        return per_opinion


if __name__ == '__main__':
    train = pd.read_csv('Train.csv', sep=';')
    test = pd.read_csv('TestX.csv', sep=';')
    preds = pd.read_csv('predictions_cls.csv', sep=',')

    rkr = ReplaceKnownRatings(preds, train)
    corrected = rkr.get_corrected_predictions_for_column(max_nr_of_different_ratings=2, min_opinion_length=1, rating_column='rate1')
