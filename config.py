SEED = 420
target_col = {'cls': 'rate1', 'reg': 'rate'}
target_col_name = 'target'
model_logs_file = './model_logs.txt'
# model_weights_save_path = './model_weights.pytorch'
model_weights_save_path = '/kaggle/working/model_weights.pytorch'
checkpoint_model_path = './model_weights.pytorch'
logging_interval = 200
load_pred_data = True # if False loads ens data as test

train_data_path = '/kaggle/input/pw-drug-review/Train.csv'
samples_to_predict_path = '/kaggle/input/pw-drug-review/TestX.csv'
preds_save_path = '/kaggle/working/'

# train_data_path = './data/Train.csv'
# samples_to_predict_path = './data/TestX.csv'
# preds_save_path = './predictions/'

ds_names = ['train', 'test', 'ens_test']
ordered_sets = ['ordered_train', 'test', 'ens_test']
target_to_number = {'high': 2, 'medium': 1, 'low': 0}
number_to_target = {2: 'high', 1: 'medium', 0: 'low'}
min_reg_class = 1
max_reg_class = 10

train_frac = 0.8
test_frac = 0.1
ensemble_frac = 0.1

n_samples = None
epochs = 7
bs = 64

### text
opinion_length_cutoff = 10
feature_min_freq = 5 # minial number of drug/condition occurrences to have an embedding
unk_drug_token = "unk_drug"
unk_condition_token = "unk_condition"

### basic gru
max_vocab = 20000
word_min_freq = 5
spacy_model = "en_core_web_sm"

BOS, EOS, UNK, PAD = '[BOS]', '[EOS]', '[UNK]', '[PAD]'
TK_MAJ, TK_UP, TK_REP, TK_WREP = 'xxmaj', 'xxup', 'xxrep', 'xxwrep'
TEXT_SPEC_TOK = [PAD, UNK, BOS, EOS, TK_MAJ, TK_UP, TK_REP, TK_WREP]
pad_token_idx = 0
unk_token_idx = 1

### RNN
rnn_hidden_size = 256
rnn_output_size = 3
rnn_n_layers = 2
bidirectional = True
rnn_dropout = 0.20
feature_dropout = 0.0
rnn_embedding_dim = 768 # 768 is pretraiend bert embedding size

### fastai
lm_vocab_path = '/kaggle/input/pw-drug-review-lm/vocab_fit_lm'
encoder_path = {'path': '/kaggle/input/pw-drug-review-lm', 'file': 'encoder_fine_tuned_drug_review_lm4ep'}

# lm_vocab_path = './data/fastai_lm/vocab_fit_lm'
# encoder_path = {'path': './data/fastai_lm/', 'file': 'encoder_fine_tuned_drug_review_lm4ep'}
