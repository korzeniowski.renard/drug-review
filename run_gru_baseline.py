from pipeline import single_model_pipeline
from utils import utilities

utilities.set_random_seed()

exp = single_model_pipeline.Experiment.from_gru_cls()
exp.train_model()

# checkpoint_model_path = './model_weights2.pytorch'#"./data/gru_baseline/model_weights.pytorch"
# exp = single_model_pipeline.Experiment.from_gru_checkpoint(checkpoint_model_path=checkpoint_model_path)
preds = exp.predict_all()
# print(preds)
