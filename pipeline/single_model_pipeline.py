import torch
from torch import optim
from tqdm import tqdm
import pandas as pd
from fastai import basic_data

import config
from utils import utilities
from data_processing import databunch as db
from data_processing import datasets
from metrics import single_model_metrics
from models import bert

#TODO
# - how to save tensordboard metrics
# - checking what is the cause of biggest loss would be a good idea
# - check how many unknow drugs/conds are in test/ens_test/to_preds
# - less paramters in head
# - regularize vanilla lstm (cuz it has 97% on train set and 75 on test)
# - create ensembles
# - load and make proba_preds for trian/test/ens_test with each model (ens training, blending)
# - add prediction
# - make sure for predictions the train/test/ens_test examples are order

# - check if prediction order is correct for bert and fastai


class Experiment:
    def __init__(self, databunch, model, loss_fn, optimizer, metric_fn, epochs, device):
        self.databunch = databunch
        self.optimizer = optimizer
        self.loss_fn = loss_fn
        self.epochs = epochs
        self.device = device
        self.model = model
        self.metric_fn = metric_fn
        self.logging_interval = config.logging_interval
        self.best_eval_loss = float('inf')

    def train_model(self):
        for epoch in range(self.epochs):
            self.model.train()
            self.run_epoch(epoch=epoch, phase='train')
            self.model.eval()
            with torch.no_grad():
                eval_loss = self.run_epoch(epoch=epoch, phase='test')

            self.save_checkpoint(eval_loss)

    def run_epoch(self, phase, epoch):
        running_loss = 0
        running_acc = 0
        step = 0

        preds = []
        labels = []
        for batch in tqdm(self.databunch[phase]):
            y_hat = self.model.forward(text=batch.text, drug=batch.drug, condition=batch.condition).squeeze(1)
            preds.append(y_hat.detach())
            labels.append(batch.label.detach())
            loss = self.loss_fn(y_hat, batch.label)

            if phase == 'train':
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

            mean_batch_loss = self.get_batch_mean_loss(loss.detach())
            running_loss = self.calcaulte_running_value(val=mean_batch_loss, running_val=running_loss, step=step)
            acc = self.metric_fn.get_metric(y_hat.detach(), batch.label.detach())
            running_acc = self.calcaulte_running_value(val=acc, running_val=running_acc, step=step)

            if step % self.logging_interval == 0 and step:
                self.log_results(phase, epoch, running_loss, running_acc)
            step += 1

        return running_loss

    def predict(self, ds_type):
        self.model.eval()
        preds = []
        labels = []
        running_acc = 0
        step = 0
        for batch in tqdm(self.databunch[ds_type]):
            y_hat = self.model.forward(text=batch.text, drug=batch.drug, condition=batch.condition).squeeze(1)
            preds.append(y_hat.detach())
            labels.append(batch.label.detach())

            acc = self.metric_fn.get_metric(y_hat.detach(), batch.label.detach())
            running_acc = self.calcaulte_running_value(val=acc, running_val=running_acc, step=step)

            step += 1
        return preds

    def predict_all(self, preds_save_path=config.preds_save_path):
        idx = 0
        df_preds = get_empty_cls_preds_df()
        model_name = self.model.__class__.__name__
        for ds_type in (['train'] + config.ordered_sets):
            prob_preds = self.predict(ds_type=ds_type)
            for pred in prob_preds:
                df_preds.loc[idx] = [model_name, ds_type, *pred.cpu().numpy()]
                idx += 1
        df_preds.to_csv(preds_save_path + f"predictions_{model_name}.csv")

    def calcaulte_running_value(self, val, running_val, step):
        return val / (step + 1) + step * running_val / (step + 1)

    def get_batch_mean_loss(self, loss):
        return loss / config.bs

    def log_results(self, mode, epoch, running_loss, running_acc, prefix=""):
        losses = self.get_log_epoch_loss(mode, epoch, running_loss, running_acc)
        print(prefix + losses)
        with open(config.model_logs_file, "a") as f:
            f.write(prefix + losses)

    def get_log_epoch_loss(self, mode, epoch, running_loss, running_acc):
        return f"\nMode: {mode}. Epoch: {epoch + 1}/{config.epochs}." \
               f" {self.metric_fn.name}: {running_acc}, CrossEntropyLoss: {running_loss}\n"

    def save_checkpoint(self, eval_loss):
        if self.best_eval_loss > eval_loss:
            self.best_eval_loss = eval_loss
            save_dict = {
                "model": self.model.state_dict(),
                "optimizer": self.optimizer.state_dict(),
                "config": {k: v for k, v in config.__dict__.items() if not k.startswith("__")}
            }
            torch.save(save_dict, config.model_weights_save_path)

    @classmethod
    def from_bert_cls(cls):
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        databunch = db.DataBunch.from_config(
            dataset_cls=datasets.BertDrugReview, seed=config.SEED, mode='cls', device=device)

        drug_num = databunch.get_num_drugs()
        condition_num = databunch.get_num_conditions()

        model = bert.BERTGRUSentiment.from_config(drug_num=drug_num, condition_num=condition_num)
        optimizer = optim.Adam(model.parameters())
        loss_fn = torch.nn.CrossEntropyLoss()
        metric_fn = single_model_metrics.BinaryAccuracy()

        model = model.to(device)
        loss_fn = loss_fn.to(device)

        return cls(
            databunch=databunch,
            model=model,
            loss_fn=loss_fn,
            optimizer=optimizer,
            metric_fn=metric_fn,
            epochs=config.epochs,
            device=device,
        )

    @classmethod
    def from_gru_cls(cls):
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        databunch = db.DataBunch.from_config(
            dataset_cls=datasets.GruDrugReview, seed=config.SEED, mode='cls', device=device)

        word_num = databunch.get_word_num()
        drug_num = databunch.get_num_drugs()
        condition_num = databunch.get_num_conditions()

        model = bert.BaselineRandomWordVectors.from_config(
            word_num=word_num, drug_num=drug_num, condition_num=condition_num)
        optimizer = optim.Adam(model.parameters())
        loss_fn = torch.nn.CrossEntropyLoss()
        metric_fn = single_model_metrics.BinaryAccuracy()

        model = model.to(device)
        loss_fn = loss_fn.to(device)

        return cls(
            databunch=databunch,
            model=model,
            loss_fn=loss_fn,
            optimizer=optimizer,
            metric_fn=metric_fn,
            epochs=config.epochs,
            device=device,
        )


    @classmethod
    def from_gru_checkpoint(cls, checkpoint_model_path=config.checkpoint_model_path):
        device = utilities.get_device()
        checkpoint = torch.load(checkpoint_model_path, map_location=device)

        # if seed split is different on some machine then it does not work
        databunch = db.DataBunch.from_config(
            dataset_cls=datasets.GruDrugReview,
            seed=checkpoint['config']['SEED'],
            mode='cls', device=device
        )
        word_num = databunch.get_word_num()
        drug_num = databunch.get_num_drugs()
        condition_num = databunch.get_num_conditions()

        model = bert.BaselineRandomWordVectors.from_config(
            word_num=word_num,
            drug_num=drug_num,
            condition_num=condition_num,
        )
        # not sure if weight will be frozen and print check
        model.load_state_dict(checkpoint['model'], strict=True)
        print(model.parameters())

        optimizer = optim.Adam(model.parameters())
        optimizer.load_state_dict(checkpoint['optimizer'])

        loss_fn = torch.nn.CrossEntropyLoss()
        metric_fn = single_model_metrics.BinaryAccuracy()

        model = model.to(device)
        loss_fn = loss_fn.to(device)

        return cls(
            databunch=databunch,
            model=model,
            loss_fn=loss_fn,
            optimizer=optimizer,
            metric_fn=metric_fn,
            epochs=config.epochs,
            device=device,
        )

    @classmethod
    def from_bert_checkpoint(cls, checkpoint_model_path=config.checkpoint_model_path):
        device = utilities.get_device()
        checkpoint = torch.load(checkpoint_model_path, map_location=device)

        # if seed split is different on some machine then it does not work
        databunch = db.DataBunch.from_config(
            dataset_cls=datasets.BertDrugReview,
            seed=checkpoint['config']['SEED'],
            mode='cls', device=device
        )
        drug_num = databunch.get_num_drugs()
        condition_num = databunch.get_num_conditions()

        model = bert.BERTGRUSentiment.from_config(drug_num=drug_num, condition_num=condition_num)
        # not sure if weight will be frozen and print check
        model.load_state_dict(checkpoint['model'], strict=True)
        print(model.parameters())

        optimizer = optim.Adam(model.parameters())
        optimizer.load_state_dict(checkpoint['optimizer'])

        loss_fn = torch.nn.CrossEntropyLoss()
        metric_fn = single_model_metrics.BinaryAccuracy()

        model = model.to(device)
        loss_fn = loss_fn.to(device)

        return cls(
            databunch=databunch,
            model=model,
            loss_fn=loss_fn,
            optimizer=optimizer,
            metric_fn=metric_fn,
            epochs=config.epochs,
            device=device,
        )


def train_fastai(learn):
    learn.fit_one_cycle(1, 2e-2, moms=(0.8, 0.7))

    learn.freeze_to(-2)
    learn.fit_one_cycle(1, slice(1e-2/(2.6**4), 1e-2), moms=(0.8, 0.7))

    learn.freeze_to(-3)
    learn.fit_one_cycle(1, slice(5e-3/(2.6**4), 5e-3), moms=(0.8, 0.7))

    learn.unfreeze()
    learn.fit_one_cycle(2, slice(1e-3/(2.6**4), 1e-3), moms=(0.8, 0.7))

    learn.fit_one_cycle(4, slice(1e-3/(2.6**4), 1e-3), moms=(0.8, 0.7))
    return learn


def train_fastai_lm(learn):
    learn.fit_one_cycle(1, 4e-2, moms=(0.8, 0.7))
    learn.unfreeze()
    learn.fit_one_cycle(4, 1e-3, moms=(0.8, 0.7))

    learn.save_encoder(config.encoder_path)
    learn.save('fine_tuned_drug_review_lm4ep')


def predict_ens_cls_fastai(
        learn,
        save_path=config.preds_save_path,
        ds_names=config.ordered_sets,
        ds_types=(
                basic_data.DatasetType.Fix,
                basic_data.DatasetType.Valid,
                basic_data.DatasetType.Test
        )
):
    idx = 0
    df_preds = get_empty_cls_preds_df()
    model_name = "fastai_model"
    for ds_type, ds_name in zip(ds_types, ds_names):
        prob_preds = learn.get_preds(ds_type=ds_type, ordered=True)[0]
        y_hats = prob_preds.argmax(dim=1)
        for pred, y_hat in zip(prob_preds, y_hats):
            df_preds.loc[idx] = [ds_name, config.number_to_target(y_hat.numpy())]
            idx += 1
    df_preds.to_csv(save_path + f"/predictions_{model_name}.csv")


def predict_ens_reg_fastai(learn, save_path=config.preds_save_path):
    ds_types = [basic_data.DatasetType.Fix, basic_data.DatasetType.Valid, basic_data.DatasetType.Test]
    df_preds = get_empty_reg_preds_df()
    idx = 0
    model_name = "fastai_model"
    for ds_type, ds_name in zip(ds_types, config.ordered_sets):
        y_hats = learn.get_preds(ds_type=ds_type, ordered=True)[0]
        y_hats = torch.clamp(y_hats, min=config.min_reg_class, max=config.max_reg_class)
        for y_hat in y_hats:
            df_preds.loc[idx] = [ds_name, y_hat.numpy()[0]]
            idx += 1
    df_preds.to_csv(save_path + f"/predictions_{model_name}.csv")


def submission_predict(learn, mode, ds_type=basic_data.DatasetType.Test):
    y_hat = learn.get_preds(ds_type=ds_type, ordered=True)[0].cpu()
    if mode == 'cls':
        y_hat = y_hat.argmax(dim=1)
        y_hat = [config.number_to_target[y.item()] for y in y_hat]
    elif mode == 'reg':
        y_hat = torch.clamp(y_hat, min=config.min_reg_class, max=config.max_reg_class).numpy()
    return y_hat


def get_empty_cls_preds_df():
    return pd.DataFrame(columns=["ds_type", config.target_col['cls']])

def get_empty_reg_preds_df():
    return pd.DataFrame(columns=["ds_type", config.target_col['reg']])
