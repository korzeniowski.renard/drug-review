import os
import pickle
from abc import abstractmethod

import pandas as pd
import torch
import sklearn

import config
from data_processing import databunch as db
from data_processing import datasets
from utils import utilities


# https://www.analyticsvidhya.com/blog/2018/06/comprehensive-guide-for-ensemble-models/
class EnsemblePipeline:
    def __init__(self, data):
        self.data = data

    @staticmethod
    def load_data(device):
        """correct splits, sampling and shit"""
        return db.DataBunch.from_config(
            dataset_cls=datasets.GruDrugReview, seed=config.SEED, mode='cls', device=device)


class NonSamplingEnsemblePipeline(EnsemblePipeline):
    def __init__(self, predictions, data):
        super().__init__(data)
        self.predictions = predictions

    @abstractmethod
    def predict(self, ds_type):
        pass

    def score(self):
        for ds_type in pd.unique(self.predictions["ds_type"]):
            y = [s.label for s in self.data[ds_type].dataset]
            y_hat = self.predict(ds_type)
            score = sklearn.metrics.accuracy_score(y, y_hat)
            print(f"ds_type: {ds_type}, acc score: {score}")

    @classmethod
    def from_predictions(cls, predictions_paths):
        device = utilities.get_device()
        data = cls.load_data(device)
        df_preds = pd.DataFrame()
        for path in predictions_paths:
            predictions = pd.read_csv(path)
            df_preds = pd.concat([df_preds, predictions])
        return cls(predictions=df_preds, data=data)


class SamplingEnsemblePipelines(EnsemblePipeline):
    def __init__(self, predictors, data):
        super().__init__(data)
        self.predictors = predictors

    @classmethod
    def from_models(cls, predictors_paths):
        predictors = {}
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        data = cls.load_data(device)
        for path in predictors_paths:
            predictor_name = os.path.basename(path)\
                .replace("predictor_", "")\
                .replace(".pkl", "")
            with open(path, "rb") as f:
                predictor = pickle.load(f)
            predictors[predictor_name] = predictor
        return cls(predictors=predictors, data=data)


class MaxVoter(NonSamplingEnsemblePipeline):
    def predict(self, ds_type):
        total_preds = []
        for model in pd.unique(self.predictions['model_name']):
            model_preds = self.predictions[
                (self.predictions["model_name"] == model) &
                (self.predictions["ds_type"] == ds_type)
            ]
            model_preds = model_preds[["cls1_prob", "cls2_prob", "cls3_prob"]].values.argmax(axis=1)
            total_preds.append(model_preds)
        total_preds = pd.DataFrame(list(zip(*total_preds)))
        return total_preds.mode(axis=1)[0].values


class WeightedVoter(NonSamplingEnsemblePipeline):
    def __init__(self, predictions, data):
        super().__init__(predictions, data)
        self.weighter = sklearn.linear_model.LogisticRegression()

    def fit(self):
        y = [s.label for s in self.data['train'].dataset]
        total_preds = []
        for model in pd.unique(self.predictions['model_name']):
            model_preds = self.predictions[
                (self.predictions["model_name"] == model) &
                (self.predictions["ds_type"] == 'train')
            ]
            # compare results of probs vs classes
            model_preds = model_preds[["cls1_prob", "cls2_prob", "cls3_prob"]].values.argmax(axis=1)
            total_preds.append(model_preds)
        X = pd.DataFrame(list(zip(*total_preds)))
        self.weighter.fit(X, y)

    def predict(self, ds_type):
        import pdb;pdb.set_trace()
        total_preds = []
        for model in pd.unique(self.predictions['model_name']):
            model_preds = self.predictions[
                (self.predictions["model_name"] == model) &
                (self.predictions["ds_type"] == ds_type)
                ]
            # compare results of probs vs classes
            model_preds = model_preds[["cls1_prob", "cls2_prob", "cls3_prob"]].max(dim=1)
            total_preds.append(model_preds)
        # X = pd.concat(total_preds, axis=1).values()
        return self.weighter.predict(X)


class Blender(NonSamplingEnsemblePipeline):
    pass


class Bagger(SamplingEnsemblePipelines):
    pass


class Booster(SamplingEnsemblePipelines):
    pass
