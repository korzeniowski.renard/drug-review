from fastai import basic_data
from pathlib import Path

import config
from data_processing import databunch as db
from models import fastai
from pipeline import single_model_pipeline


for phase in ['train', 'test']:
    for mode in ['reg', 'cls']:
        databunch, train_df, df = db.get_fastai_databunch(
            seed=config.SEED, mode=mode, lm_vocab_path=config.lm_vocab_path, pred_data=True)
        model = fastai.get_fastai_classifier(databunch=databunch, encoder_path=config.encoder_path)
        model.path = Path(f"./data/fastai_{mode}")
        model = model.load(f"fastai_{mode}")

        if phase == 'train':
            predictions = single_model_pipeline.submission_predict(learn=model, mode=mode, ds_type=basic_data.DatasetType.Fix)
        else:
            predictions = single_model_pipeline.submission_predict(learn=model, mode=mode)

        train_df['predictions'] = predictions
        train_df = train_df.drop(columns=['target'])
        preds_root = Path(config.preds_save_path)
        save_path = preds_root / f"predictions_{phase}_{mode}.csv"
        if phase == 'train':
            train_df.to_csv(save_path)
        else:
            df.to_csv(save_path)
