from pipeline import ensemble_pipeline
from utils import utilities

utilities.set_random_seed()

prediction_paths = [
    #"./data/bert_featdrop0d2_minfreq5/predictions_BERTGRUSentiment.csv",
    "./data/fastai_cls/predictions_fastai_model.csv",
    #"./data/gru_baseline/predictions_BaselineRandomWordVectors.csv"
    #"./predictions/predictions_BaselineRandomWordVectors.csv",
    #"./predictions/predictions_BERTGRUSentiment.csv",
]

ens = ensemble_pipeline.MaxVoter.from_predictions(prediction_paths)
# ens.fit()
ens.score()
