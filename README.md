## Drug Review

This project contains algorithm that predicts a score of  
how well a drug was received by a reviewer based on his opinion.

[problem description](https://home.ipipan.waw.pl/p.teisseyre/TEACHING/ZMUM/Zadania/Projekt1.pdf)


### Ideas

TODO
- look for a way to fuzzy match condition/drug name in text
- check if sentiment is correlated with opinion (pretrained for quick check)
- look for sentiment architecture 

#### Sentiment
```
General 
opinion sentiment ----
                     |
                     + ---> class/score prediction
                     |
conditional prob  ----
of drug/cond to belong 
to given class
```

#### Text embedding
```
Text with drug/cond 
highlighted 
to predict sentiment ---
for this drug          |
                       + ---> class/score prediction
                       |
some statistics about  |
drug and cond (cond prob)                       
```

sentiment model could be used raw or fine tuned with class score as targets.

# pytorch for text
https://pytorch.org/text/
# tutorials for pytorch text 
https://github.com/bentrevett/pytorch-sentiment-analysis
# SOTA models for SST2 (binary sentiment)
https://paperswithcode.com/sota/sentiment-analysis-on-sst-2-binary
# models
https://github.com/huggingface/transformers#model-architectures

# albert
https://github.com/huggingface/transformers


### Testset 

32533 out of 50000 are repeated.