import pandas as pd

import config


class DataLoader:
    def __init__(self, train_path, samples_to_predict_path, train_frac,
                 test_frac, ensemble_frac, opinion_length_cutoff, mode, seed):
        self.train_path = train_path
        self.samples_to_predict_path = samples_to_predict_path
        self.seed = seed
        self.opinion_length_cutoff = opinion_length_cutoff
        self.train_frac = train_frac
        self.test_frac = test_frac
        self.ensemble_frac = ensemble_frac
        self.mode = mode

    def load_training_data(self):
        """
        if separate model with condition name is introduced
        df['condition'][df['condition'].str.contains('</span> users found this comment helpful.')] = "unknown"
        """
        df = pd.read_csv(self.train_path, sep=";", nrows=config.n_samples)
        df = self.drop_invalid_samples(df)
        df = self.set_target_col(df)
        dfs = self.split_train_test_ensemble(df)
        dfs = self.add_test_data(dfs, config.load_pred_data)
        return dfs

    def load_samples_to_predict(self):
        """load the file and let the dataset preprocess the text with pipeline fitted on trainset"""
        return pd.read_csv(self.samples_to_predict_path, sep=";")

    def load_all_text(self):
        """
        load text from train and test set for transfer learnining (finetuning context embedding models, fastai)
        might not be benefitial if langauge latent space is different than realations between review and rate
        """
        train_text = pd.read_csv(self.train_path, sep=";", nrows=config.n_samples)[['opinion']]
        test_text = pd.read_csv(self.samples_to_predict_path, sep=";", nrows=config.n_samples)[['opinion']]
        df = pd.concat([train_text, test_text])
        train, test = self.sample_data(df, frac=0.2)
        return {'train': train, 'test': test}

    def split_train_test_ensemble(self, df, prediction_mode=config.load_pred_data):
        if prediction_mode:
            data_dict = {'train': df, 'test': df.iloc[:10], 'ens_test': df.iloc[:10]}
        else:
            print("Splitting long opinion duplicates to the same train/test/ens_test sets")

            duplicates = df[df['opinion'].duplicated(keep=False)]
            unique = df[~df['opinion'].duplicated(keep=False)]

            # we dont want generic opinions like "good drug!" to be treated as duplicated opinions
            long_duplicates = duplicates[
                duplicates['opinion'].str.split(" ").apply(len) > self.opinion_length_cutoff].reset_index()
            grouped_dup_idxs = long_duplicates[
                ['opinion', 'index']].groupby(['opinion'])['index'].apply(list).reset_index(drop=True)

            unique = self.sample_sets(unique)
            duplicates = [df[df.index.isin(self.unpack_idx(idx))] for idx in self.sample_sets(grouped_dup_idxs)]

            train, test, ensemble_test = [pd.concat([u, d]) for u, d in zip(unique, duplicates)]
            print(
                f"After dropping duplicates train size: {len(train)},"
                f" test size: {len(test)}, ens_test size: {len(ensemble_test)}")
            data_dict = {'train': train, 'test': test, 'ens_test': ensemble_test}
        return data_dict

    def drop_invalid_samples(self, df):
        before_droping = len(df)
        df = df.dropna().reset_index(drop=True)
        df = df[~df.duplicated()]
        df = df[~df['condition'].str.contains('</span> users found this comment helpful.')]
        after_droping = len(df)
        print(
            f"Sample before dropping nans and duplicates {before_droping}. "
            f"After cleaning nans {after_droping}. "
            f"Procentage dropped {1-after_droping/before_droping}"
        )
        return df

    def sample_sets(self, data):
        train, not_train = self.sample_data(data, frac=self.train_frac)
        test, ensemble_test = self.sample_data(not_train, frac=self.test_frac/(self.test_frac + self.ensemble_frac))
        return train, test, ensemble_test

    def sample_data(self, data, frac):
        set_frac = data.sample(frac=frac, random_state=self.seed)
        set_complement_frac = data[~data.index.isin(set_frac.index)]
        return set_frac, set_complement_frac

    def unpack_idx(self, idxs_df):
        all_idx = list(zip(*idxs_df.values))[0]
        return all_idx

    def set_target_col(self, df):
        df[config.target_col_name] = df[config.target_col[self.mode]]
        df = df.drop(columns=[config.target_col['cls'], config.target_col['reg']])

        if self.mode == 'cls':
            df = self.numerize_targets(df)
        elif self.mode == 'reg':
            df[config.target_col_name] = df[config.target_col_name].astype('float32')
        else:
            raise ValueError(f"mode {self.mode} is not supported")
        return df

    def add_test_data(self, dfs, is_extend):
        if is_extend:
            df = pd.read_csv(config.samples_to_predict_path, sep=";", nrows=config.n_samples)
            dfs['preds'] = df
        return dfs

    def numerize_targets(self, df):
        df[config.target_col_name] = df[config.target_col_name].map(config.target_to_number)
        return df

    @classmethod
    def from_config(cls, seed, mode):
        return cls(
            train_path=config.train_data_path,
            samples_to_predict_path=config.samples_to_predict_path,
            train_frac=config.train_frac,
            test_frac=config.test_frac,
            ensemble_frac=config.ensemble_frac,
            opinion_length_cutoff=config.opinion_length_cutoff,
            seed=seed,
            mode=mode,
        )
