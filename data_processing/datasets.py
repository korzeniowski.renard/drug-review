from abc import abstractmethod

import torch
from torchtext import data

import config
from data_processing import text_processing


class DrugReview(data.Dataset):
    def __init__(self, df, text_field, label_field, drug_field, condition_field, **kwargs):
        fields = [('text', text_field), ('label', label_field), ('drug', drug_field), ('condition', condition_field)]
        examples = []

        for _, row in df.iterrows():
            text, label, drug, condition = row['opinion'], row[config.target_col_name], row['name'], row['condition']
            examples.append(data.Example.fromlist([text, label, drug, condition], fields))

        super().__init__(examples, fields, **kwargs)

    @staticmethod
    def sort_key(ex):
        return len(ex.text)

    @staticmethod
    def get_label_field():
        return data.LabelField(dtype=torch.long)

    @staticmethod
    def get_feature_fields(token_to_idx_converter, unk_token):
        return data.Field(
            sequential=False,
            unk_token=unk_token,
            tokenize=lambda x: x,
            use_vocab=False,
            preprocessing=token_to_idx_converter
        )

    @staticmethod
    def get_val_to_idx_converter(col, unk_token):
        return text_processing.DefaultIdxVocab.from_tokens(
            tokens=col.values, unk_token=unk_token, min_freq=config.feature_min_freq)


class BertDrugReview(DrugReview):
    @staticmethod
    def get_tokenize_fn(tokenizer, max_input_length):
        def tokenize_and_cut(sentence):
            tokens = tokenizer.tokenize(sentence)
            tokens = tokens[:max_input_length - 2]
            return tokens

        return tokenize_and_cut

    @staticmethod
    def get_word_field(tokenizer):
        max_input_length = tokenizer.max_model_input_sizes['bert-base-uncased']
        init_token_idx = tokenizer.cls_token_id
        eos_token_idx = tokenizer.sep_token_id
        pad_token_idx = tokenizer.pad_token_id
        unk_token_idx = tokenizer.unk_token_id

        text_field = data.Field(
            tokenize=BertDrugReview.get_tokenize_fn(tokenizer=tokenizer, max_input_length=max_input_length),
            preprocessing=tokenizer.convert_tokens_to_ids,
            batch_first=True,
            use_vocab=False,
            init_token=init_token_idx,
            eos_token=eos_token_idx,
            pad_token=pad_token_idx,
            unk_token=unk_token_idx
        )

        return text_field

    @classmethod
    def from_df(cls, df, feature_fields=None, **kwargs):
        tokenizer = text_processing.get_pretrained_bert_tokenizer()
        text_field = cls.get_word_field(tokenizer)
        label_field = cls.get_label_field()
        if feature_fields:
            drug_field, condition_field = feature_fields['drug'], feature_fields['condition']
        else:
            drug_to_idx_converter = cls.get_val_to_idx_converter(df['name'], config.unk_drug_token)
            drug_field = cls.get_feature_fields(drug_to_idx_converter, config.unk_drug_token)
            condition_to_idx_converter = cls.get_val_to_idx_converter(df['condition'], config.unk_condition_token)
            condition_field = cls.get_feature_fields(condition_to_idx_converter, config.unk_condition_token)

        label_field.build_vocab([0,0,0, 1,1, 2])

        return cls(df, text_field, label_field, drug_field, condition_field, **kwargs)


class GruDrugReview(DrugReview):
    @staticmethod
    def get_word_field(tokenizer, text_vocab):
        text_field = data.Field(
            tokenize=tokenizer,
            preprocessing=text_vocab,
            batch_first=True,
            use_vocab=False,
            pad_token=config.pad_token_idx,
        )

        return text_field

    @staticmethod
    def get_word_to_idx_converter(col, tokenizer):
        tokens = col.apply(tokenizer)
        return text_processing.Vocab.from_tokens(
            tokens=tokens, min_freq=config.word_min_freq, max_vocab=config.max_vocab)

    @classmethod
    def from_df(cls, df, feature_fields=None, **kwargs):
        label_field = cls.get_label_field()
        label_field.build_vocab([0,0,0, 1,1, 2])

        if feature_fields:
            drug_field, condition_field, text_field = \
                feature_fields['drug'], feature_fields['condition'], feature_fields['text']
        else:
            tokenizer = text_processing.Tokenizer()
            text_vocab = cls.get_word_to_idx_converter(df['opinion'], tokenizer)
            text_field = cls.get_word_field(tokenizer=tokenizer, text_vocab=text_vocab)

            drug_to_idx_converter = cls.get_val_to_idx_converter(df['name'], config.unk_drug_token)
            drug_field = cls.get_feature_fields(drug_to_idx_converter, config.unk_drug_token)
            condition_to_idx_converter = cls.get_val_to_idx_converter(df['condition'], config.unk_condition_token)
            condition_field = cls.get_feature_fields(condition_to_idx_converter, config.unk_condition_token)

        return cls(df, text_field, label_field, drug_field, condition_field, **kwargs)
