import pickle

from torchtext import data
from fastai import text
import pandas as pd

import config
from data_processing import data_loader
from data_processing import text_processing


class DataBunch:
    def __init__(self, train, test, ens_test, ordered_train):
        self.train = train
        self.test = test
        self.ens_test = ens_test
        self.ordered_train = ordered_train

    def __getitem__(self, mode):
        return self.__dict__[mode]

    def get_word_num(self):
        return len(self.train.dataset.fields['text'].preprocessing.itos)

    def get_num_drugs(self):
        return len(self.train.dataset.fields['drug'].preprocessing.itos)

    def get_num_conditions(self):
        return len(self.train.dataset.fields['condition'].preprocessing.itos)

    @classmethod
    def from_config(cls, dataset_cls, seed, mode, device):
        loader = data_loader.DataLoader.from_config(seed=seed, mode=mode)
        dfs = loader.load_training_data()

        dfs = text_processing.clean_text_dfs(dfs)

        train_ds = dataset_cls.from_df(df=dfs['train'])
        train_feature_fields = {
            'drug': train_ds.fields['drug'],
            'condition': train_ds.fields['drug'],
            'text': train_ds.fields['text'],
        }

        test_ds = dataset_cls.from_df(df=dfs['test'], feature_fields=train_feature_fields)
        ens_test_ds = dataset_cls.from_df(df=dfs['ens_test'], feature_fields=train_feature_fields)

        train_iterator = data.BucketIterator(
            train_ds,
            batch_size=config.bs,
            device=device,
            sort=True,
            train=True,
            shuffle=True,
            sort_within_batch=True,
        )

        ordered_sets = ['ordered_train', 'test', 'ens_test']
        df_sets = [train_ds, test_ds, ens_test_ds]

        ordered_iterators = {ds_type: data.BucketIterator(
                df,
                batch_size=config.bs,
                device=device,
                train=False,
                repeat=False,
                shuffle=False,
                sort=False,
        ) for ds_type, df in zip(ordered_sets, df_sets)}

        return cls(train=train_iterator, **ordered_iterators)


def get_fastai_databunch(seed, mode, lm_vocab_path, pred_data=config.load_pred_data):
    with open(lm_vocab_path, 'rb') as f:
        lm_vocab = pickle.load(f)
    loader = data_loader.DataLoader.from_config(seed=seed, mode=mode)
    dfs = loader.load_training_data()
    db = text.TextDataBunch.from_df(
        path='.',
        train_df=dfs['train'],
        valid_df=dfs['test'],
        test_df=dfs['preds'] if pred_data else dfs['ens_test'],
        text_cols='opinion',
        label_cols=config.target_col_name,
        vocab=lm_vocab,
        bs=config.bs,
    )
    if pred_data:
        return db, dfs['train'], dfs['preds']
    else:
        return db


def get_fastai_lm_databunch(seed, mode):
    loader = data_loader.DataLoader.from_config(seed=seed, mode=mode)
    dfs = loader.load_all_text()
    databunch = text.TextLMDataBunch.from_df(
        path='.',
        train_df=dfs['train'],
        valid_df=dfs['test'],
        text_cols='opinion',
        bs=config.bs,
    )
    with open(config.lm_vocab_path, 'wb') as f:
        pickle.dump(databunch.vocab, f)
    return databunch
