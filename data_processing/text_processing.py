import collections
import html
import re

import spacy
from transformers import BertTokenizer

import config

## bert text preprocesing


def clean_text_dfs(dfs):
    for k in dfs.keys():
        dfs[k]['opinion'] = clean_text(dfs[k]['opinion'])
    return dfs


def rm_useless_spaces(t):
    "Remove multiple spaces in `t`."
    return re.sub(' {2,}', ' ', t)


def fix_html(x:str) -> str:
    "List of replacements from html strings in `x`."
    re1 = re.compile(r'  +')
    x = x.replace('#39;', "'").replace('amp;', '&').replace('#146;', "'").replace(
        'nbsp;', ' ').replace('#36;', '$').replace('\\n', "\n").replace('quot;', "'").replace(
        '<br />', "\n").replace('\\"', '"').replace('<unk>', 'unknown').replace(' @.@ ','.').replace(
        ' @-@ ','-').replace(' @,@ ',',').replace('\\', ' \\ ')
    return re1.sub(' ', html.unescape(x))


def clean_text(text):
    text = text.apply(fix_html)
    text = text.str.replace('"', ' ')
    text = text.apply(rm_useless_spaces)
    return text


def get_pretrained_bert_tokenizer():
    return BertTokenizer.from_pretrained('bert-base-uncased')


def get_bert_tokenize_fn(tokenizer, max_input_length):
    def tokenize_and_cut(sentence):
        tokens = tokenizer.tokenize(sentence)
        tokens = tokens[:max_input_length-2]
        return tokens
    return tokenize_and_cut


class DefaultIdxVocab:
    def __init__(self, itos):
        self.itos = itos
        self.stoi = collections.defaultdict(int, {v: k for k, v in enumerate(self.itos)})

    def get_vocab_size(self):
        return len(self.itos)

    def numericalize(self, t):
        return self.stoi[t]

    def textify(self, num):
        return self.itos[num] if num <= len(self.itos) else self.stoi[0]

    def __getstate__(self):
        return {'itos': self.itos}

    def __call__(self, token):
        return self.numericalize(token)

    def __setstate__(self, state:dict):
        self.itos = state['itos']
        self.stoi = collections.defaultdict(int, {v: k for k, v in enumerate(self.itos)})

    @classmethod
    def from_tokens(cls, tokens, min_freq, unk_token):
        freq = collections.Counter(p for p in tokens)
        itos = [o for o, c in freq.most_common() if c >= min_freq]
        itos.insert(config.unk_token_idx, unk_token)
        return cls(itos)


## basic text preprocessing


def spec_add_spaces(t):
    "Add spaces around / and # in `t`. \n"
    return re.sub(r'([/#\n])', r' \1 ', t)


def replace_all_caps(x):
    "Replace tokens in ALL CAPS in `x` by their lower version and add `TK_UP` before."
    res = []
    for t in x:
        if t.isupper() and len(t) > 1: res.append(config.TK_UP); res.append(t.lower())
        else: res.append(t)
    return res


def deal_caps(x):
    "Replace all Capitalized tokens in `x` by their lower version and add `TK_MAJ` before."
    res = []
    for t in x:
        if t == '': continue
        if t[0].isupper() and len(t) > 1 and t[1:].islower(): res.append(config.TK_MAJ)
        res.append(t.lower())
    return res


TEXT_PRE_RULES = [spec_add_spaces]
TEXT_POST_RULES = [replace_all_caps, deal_caps]


class Tokenizer:
    def __init__(self):
        self.tok_fn = spacy.load(config.spacy_model, disable=["parser", "tagger", "ner"])
        self.pre_special_cases = TEXT_PRE_RULES
        self.post_special_cases = TEXT_POST_RULES

    def tokenize(self, text):
        return [tok.text for tok in self.tok_fn(text)]

    def __call__(self, text):
        return self.tokenize(text)


class Vocab:
    """FastAI https://github.com/fastai/fastai/blob/master/fastai/text/transform.py#L128"""
    def __init__(self, itos):
        self.itos = itos
        self.stoi = collections.defaultdict(int, {v: k for k, v in enumerate(self.itos)})

    def get_vocab_size(self):
        return len(self.itos)

    def numericalize(self, t):
        return [self.stoi[w]for w in t]

    def textify(self, nums, sep=' '):
        return sep.join([self.itos[i] for i in nums])

    def __call__(self, token):
        return self.numericalize(token)

    def __getstate__(self):
        return {'itos': self.itos}

    def __setstate__(self, state:dict):
        self.itos = state['itos']
        self.stoi = collections.defaultdict(int, {v: k for k, v in enumerate(self.itos)})

    @classmethod
    def from_tokens(cls, tokens, max_vocab, min_freq):
        freq = collections.Counter(p for o in tokens for p in o)
        itos = [o for o, c in freq.most_common(max_vocab) if c >= min_freq]
        for o in reversed(config.TEXT_SPEC_TOK):
            if o in itos: itos.remove(o)
            itos.insert(0, o)
        itos = itos[:max_vocab]
        return cls(itos)
