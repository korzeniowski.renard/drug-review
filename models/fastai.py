from fastai import text
from fastai import layers


def get_fastai_classifier(databunch, encoder_path):
    learn = text.text_classifier_learner(
        databunch, text.models.AWD_LSTM, drop_mult=0.5, path=encoder_path['path'], model_dir="")
    learn.load_encoder(encoder_path['file'])
    return learn


def get_fastai_regressor(databunch, encoder_path):
    learn = text.text_classifier_learner(
        databunch, text.models.AWD_LSTM, drop_mult=0.5, path=encoder_path['path'], model_dir="")
    learn.load_encoder(encoder_path['file'])
    learn.loss_func = layers.MSELossFlat()
    return learn


def get_fastai_lm(databunch):
    return text.language_model_learner(databunch, text.models.AWD_LSTM, pretrained=True, drop_mult=0.3)
