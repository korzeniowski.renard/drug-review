import torch
import torch.nn as nn
from transformers import BertModel

import config

## fc instead of gru
# drug/cond embedding size = 10     | concat all 3
# text embed 64 -> gru 128 -> fc 10 | and fc on them

## try 1 layer gru and smaller embedding size


class BERTGRUSentiment(nn.Module):
    def __init__(
        self,
        drug_num,
        condition_num,
        hidden_dim,
        output_dim,
        n_layers,
        bidirectional,
        dropout,
        feature_dropout,
    ):
        super().__init__()
        self.bert = BertModel.from_pretrained('bert-base-uncased')
        embedding_dim = self.bert.config.to_dict()['hidden_size']

        self.drugs_embeddings = nn.Embedding(num_embeddings=drug_num, embedding_dim=embedding_dim)
        self.conditions_embeddings = nn.Embedding(num_embeddings=condition_num, embedding_dim=embedding_dim)

        self.rnn = nn.GRU(
            embedding_dim,
            hidden_dim,
            num_layers=n_layers,
            bidirectional=bidirectional,
            batch_first=True,
            dropout=0 if n_layers < 2 else dropout
        )
        self.fc = nn.Linear(hidden_dim * 2 if bidirectional else hidden_dim, output_dim)
        self.dropout = nn.Dropout(dropout)
        self.feature_dropout = feature_dropout
        self.softmax = nn.Softmax(dim=1)

    def forward(self, text, drug, condition):
        with torch.no_grad():
            self.bert.eval()
            text_embedding = self.bert(text)[0]

        if not self.training:
            drug = torch.empty_like(drug).bernoulli_(1-self.feature_dropout) * drug
            condition = torch.empty_like(condition).bernoulli_(1-self.feature_dropout) * condition

        drug_embedding = self.drugs_embeddings(drug).unsqueeze(dim=1)
        condition_embedding = self.conditions_embeddings(condition).unsqueeze(dim=1)

        embedding = torch.cat([drug_embedding, condition_embedding, text_embedding], dim=1)

        _, hidden = self.rnn(embedding)

        if self.rnn.bidirectional:
            hidden = self.dropout(torch.cat((hidden[-2, :, :], hidden[-1, :, :]), dim=1))
        else:
            hidden = self.dropout(hidden[-1, :, :])
        sentiment_prob = self.softmax(self.fc(hidden))
        return sentiment_prob

    def set_trainable_parameters(self):
        for name, param in self.named_parameters():
            if name.startswith('bert'):
                param.requires_grad = False

    @classmethod
    def from_config(cls, drug_num, condition_num):
        bert = cls(
            drug_num=drug_num,
            condition_num=condition_num,
            hidden_dim=config.rnn_hidden_size,
            output_dim=config.rnn_output_size,
            n_layers=config.rnn_n_layers,
            bidirectional=config.bidirectional,
            dropout=config.rnn_dropout,
            feature_dropout=config.feature_dropout,
        )
        bert.set_trainable_parameters()
        return bert


class BaselineRandomWordVectors(nn.Module):
    def __init__(
        self,
        embedding_dim,
        word_num,
        drug_num,
        condition_num,
        hidden_dim,
        output_dim,
        n_layers,
        bidirectional,
        dropout,
        feature_dropout,
    ):
        super().__init__()
        self.word_embeddings = nn.Embedding(num_embeddings=word_num, embedding_dim=embedding_dim)
        self.drugs_embeddings = nn.Embedding(num_embeddings=drug_num, embedding_dim=embedding_dim)
        self.conditions_embeddings = nn.Embedding(num_embeddings=condition_num, embedding_dim=embedding_dim)

        self.rnn = nn.GRU(
            embedding_dim,
            hidden_dim,
            num_layers=n_layers,
            bidirectional=bidirectional,
            batch_first=True,
            dropout=0 if n_layers < 2 else dropout
        )
        self.fc = nn.Linear(hidden_dim * 2 if bidirectional else hidden_dim, output_dim)
        self.dropout = nn.Dropout(dropout)
        self.feature_dropout = feature_dropout
        self.softmax = nn.Softmax(dim=1)

    def forward(self, text, drug, condition):
        text_embedding = self.word_embeddings(text)

        if not self.training:
            drug = torch.empty_like(drug).bernoulli_(1 - self.feature_dropout) * drug
            condition = torch.empty_like(condition).bernoulli_(1 - self.feature_dropout) * condition

        drug_embedding = self.drugs_embeddings(drug).unsqueeze(dim=1)
        condition_embedding = self.conditions_embeddings(condition).unsqueeze(dim=1)

        embedding = torch.cat([drug_embedding, condition_embedding, text_embedding], dim=1)

        _, hidden = self.rnn(embedding)

        if self.rnn.bidirectional:
            hidden = self.dropout(torch.cat((hidden[-2, :, :], hidden[-1, :, :]), dim=1))
        else:
            hidden = self.dropout(hidden[-1, :, :])
        sentiment_prob = self.softmax(self.fc(hidden))

        return sentiment_prob

    @classmethod
    def from_config(cls, word_num, drug_num, condition_num):
        bert = cls(
            word_num=word_num,
            drug_num=drug_num,
            condition_num=condition_num,
            hidden_dim=config.rnn_hidden_size,
            output_dim=config.rnn_output_size,
            n_layers=config.rnn_n_layers,
            bidirectional=config.bidirectional,
            dropout=config.rnn_dropout,
            feature_dropout=config.feature_dropout,
            embedding_dim=config.rnn_embedding_dim,
        )
        return bert
